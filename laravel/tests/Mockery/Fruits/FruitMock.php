<?php

namespace Tests\Mockery\Fruits;

trait FruitMock
{
    public function fruit(): array
    {
        return [
            "id" => "1",
            "scientific_name" => "Malus Domestica",
            "tree_name" => "Manzano",
            "fruit_name" => "Manzana",
            "family" => "Rosaceae",
            "origin" => "Asia Central",
            "description" => "La RosaceaeRosaceaemanzana es el fruto del manzano, árbol de la familia de las rosáceas. Es una fruta pomácea de forma redonda y sabor más o menos dulce, dependiendo de la variedad. La manzana es un árbol caducifolio, generalmente de 2 a 4,5 m (6 a 15 pies) de altura en cultivo y hasta 9 m (30 pies) en la naturaleza.",
            "bloom" => "Primavera",
            "maturation_fruit" => "Finales del verano o otoño",
            "life_cycle" => "60-80 años",
            "climatic_zone" => "Frio"
        ];
    }

    public function addFruitResult(): array
    {
        return [
            "data" => [
                "addFruit" => [
                    "id" => "1",
                    "scientific_name" => "Malus Domestica",
                    "tree_name" => "Manzano",
                    "fruit_name" => "Manzana",
                    "family" => "Rosaceae",
                    "origin" => "Asia Central",
                    "description" => "La RosaceaeRosaceaemanzana es el fruto del manzano, árbol de la familia de las rosáceas. Es una fruta pomácea de forma redonda y sabor más o menos dulce, dependiendo de la variedad. La manzana es un árbol caducifolio, generalmente de 2 a 4,5 m (6 a 15 pies) de altura en cultivo y hasta 9 m (30 pies) en la naturaleza.",
                    "bloom" => "Primavera",
                    "maturation_fruit" => "Finales del verano o otoño",
                    "life_cycle" => "60-80 años",
                    "climatic_zone" => "Frio"
                ]
            ]
        ];
    }

    public function fruitResult(): array
    {
        return [
            "data" => [
                "fruit" => [
                    "id" => "1",
                    "scientific_name" => "Malus Domestica",
                    "tree_name" => "Manzano",
                    "fruit_name" => "Manzana",
                    "family" => "Rosaceae",
                    "origin" => "Asia Central",
                    "description" => "La RosaceaeRosaceaemanzana es el fruto del manzano, árbol de la familia de las rosáceas. Es una fruta pomácea de forma redonda y sabor más o menos dulce, dependiendo de la variedad. La manzana es un árbol caducifolio, generalmente de 2 a 4,5 m (6 a 15 pies) de altura en cultivo y hasta 9 m (30 pies) en la naturaleza.",
                    "bloom" => "Primavera",
                    "maturation_fruit" => "Finales del verano o otoño",
                    "life_cycle" => "60-80 años",
                    "climatic_zone" => "Frio"
                ]
            ]
        ];
    }

    public function updateFruitResult(): array
    {
        return [
            "data" => [
                "updateFruit" => [
                    "id" => "1",
                    "scientific_name" => "Malus Domestica",
                    "tree_name" => "Manzano",
                    "fruit_name" => "Manzana",
                    "family" => "Rosaceae",
                    "origin" => "Asia Central",
                    "description" => "La RosaceaeRosaceaemanzana es el fruto del manzano, árbol de la familia de las rosáceas. Es una fruta pomácea de forma redonda y sabor más o menos dulce, dependiendo de la variedad. La manzana es un árbol caducifolio, generalmente de 2 a 4,5 m (6 a 15 pies) de altura en cultivo y hasta 9 m (30 pies) en la naturaleza.",
                    "bloom" => "Primavera",
                    "maturation_fruit" => "Finales del verano o otoño",
                    "life_cycle" => "60-80 años",
                    "climatic_zone" => "Frio"
                ]
            ]
        ];
    }

    public function deleteFruitResult(): array
    {
        return [
            "data" => [
                "deleteFruit" => [
                    "id" => null,
                    "scientific_name" => null,
                    "tree_name" => null,
                    "fruit_name" => null,
                    "family" => null,
                    "origin" => null,
                    "description" => null,
                    "bloom" => null,
                    "maturation_fruit" => null,
                    "life_cycle" => null,
                    "climatic_zone" => null
                ]
            ]
        ];
    }

    public function fruits(): array
    {
        return [
            "data" => [
                "fruits" => [
                    [
                        "id" => "1",
                        "scientific_name" => "Malus Domestica",
                        "tree_name" => "Manzano",
                        "fruit_name" => "Manzana",
                        "family" => "Rosaceae",
                        "origin" => "Asia Central",
                        "description" => "La RosaceaeRosaceaemanzana es el fruto del manzano, árbol de la familia de las rosáceas. Es una fruta pomácea de forma redonda y sabor más o menos dulce, dependiendo de la variedad. La manzana es un árbol caducifolio, generalmente de 2 a 4,5 m (6 a 15 pies) de altura en cultivo y hasta 9 m (30 pies) en la naturaleza.",
                        "bloom" => "Primavera",
                        "maturation_fruit" => "Finales del verano o otoño",
                        "life_cycle" => "60-80 años",
                        "climatic_zone" => "Frio"
                    ],
                    [
                        "id" => "2",
                        "scientific_name" => "Pyrus Communis",
                        "tree_name" => "Peral",
                        "fruit_name" => "Pera",
                        "family" => "Rosaceae",
                        "origin" => "Europa Oriental y Asia Menor",
                        "description" => "La pera es el fruto del peral, árbol de la familia de las rosáceas. El fruto es un pomo comestible de verde marron. Es una especie de árbol caducifolio, generalmente de 2 hasta 20 m de altura.",
                        "bloom" => "Otoño",
                        "maturation_fruit" => "Invierno hasta Primavera",
                        "life_cycle" => "65-400 años",
                        "climatic_zone" => "Templados, Húmedos y Frios"
                    ],
                    [
                        "id" => "3",
                        "scientific_name" => "Musa x Paradisiaca",
                        "tree_name" => "Platano",
                        "fruit_name" => "Banana",
                        "family" => "Musaceae",
                        "origin" => "Indomalaya",
                        "description" => "Es un fruto comestible, botánicamente una baya, de varios tipos de grandes plantas herbáceas. A estas plantas de gran porte que tienen aspecto de arbolillo se las denomina plataneras, bananeros, bananeras, plátanos o bananos. Es un fruto con variables en tamaño (10 cm), color y firmeza, alargado, generalmente curvado y carnoso.",
                        "bloom" => "",
                        "maturation_fruit" => "2 a 3 meses",
                        "life_cycle" => "1 año",
                        "climatic_zone" => "Calido"
                    ],
                    [
                        "id" => "4",
                        "scientific_name" => "Citrus x Limon",
                        "tree_name" => "Limonero",
                        "fruit_name" => "Limón",
                        "family" => "Rutáceae",
                        "origin" => "Asia",
                        "description" => "Es un pequeño árbol frutal perenne. Su fruto es el limón. Una fruta comestible de sabor ácido y extremadamente fragante que se usa principalmente en la alimentación. Es un árbol que a menudo con espinas, puede alcanzar los 4 m de altura, con una copa muy ramificada.",
                        "bloom" => "Otoño y Invierno",
                        "maturation_fruit" => "",
                        "life_cycle" => "50-100 años",
                        "climatic_zone" => "Mediterráneo"
                    ]
                ]
            ]
        ];
    }

    public function fruitsResult(): array
    {
        return [
            [
                "id" => "1",
                "scientific_name" => "Malus Domestica",
                "tree_name" => "Manzano",
                "fruit_name" => "Manzana",
                "family" => "Rosaceae",
                "origin" => "Asia Central",
                "description" => "La RosaceaeRosaceaemanzana es el fruto del manzano, árbol de la familia de las rosáceas. Es una fruta pomácea de forma redonda y sabor más o menos dulce, dependiendo de la variedad. La manzana es un árbol caducifolio, generalmente de 2 a 4,5 m (6 a 15 pies) de altura en cultivo y hasta 9 m (30 pies) en la naturaleza.",
                "bloom" => "Primavera",
                "maturation_fruit" => "Finales del verano o otoño",
                "life_cycle" => "60-80 años",
                "climatic_zone" => "Frio"
            ],
            [
                "id" => "2",
                "scientific_name" => "Pyrus Communis",
                "tree_name" => "Peral",
                "fruit_name" => "Pera",
                "family" => "Rosaceae",
                "origin" => "Europa Oriental y Asia Menor",
                "description" => "La pera es el fruto del peral, árbol de la familia de las rosáceas. El fruto es un pomo comestible de verde marron. Es una especie de árbol caducifolio, generalmente de 2 hasta 20 m de altura.",
                "bloom" => "Otoño",
                "maturation_fruit" => "Invierno hasta Primavera",
                "life_cycle" => "65-400 años",
                "climatic_zone" => "Templados, Húmedos y Frios"
            ],
            [
                "id" => "3",
                "scientific_name" => "Musa x Paradisiaca",
                "tree_name" => "Platano",
                "fruit_name" => "Banana",
                "family" => "Musaceae",
                "origin" => "Indomalaya",
                "description" => "Es un fruto comestible, botánicamente una baya, de varios tipos de grandes plantas herbáceas. A estas plantas de gran porte que tienen aspecto de arbolillo se las denomina plataneras, bananeros, bananeras, plátanos o bananos. Es un fruto con variables en tamaño (10 cm), color y firmeza, alargado, generalmente curvado y carnoso.",
                "bloom" => "",
                "maturation_fruit" => "2 a 3 meses",
                "life_cycle" => "1 año",
                "climatic_zone" => "Calido"
            ],
            [
                "id" => "4",
                "scientific_name" => "Citrus x Limon",
                "tree_name" => "Limonero",
                "fruit_name" => "Limón",
                "family" => "Rutáceae",
                "origin" => "Asia",
                "description" => "Es un pequeño árbol frutal perenne. Su fruto es el limón. Una fruta comestible de sabor ácido y extremadamente fragante que se usa principalmente en la alimentación. Es un árbol que a menudo con espinas, puede alcanzar los 4 m de altura, con una copa muy ramificada.",
                "bloom" => "Otoño y Invierno",
                "maturation_fruit" => "",
                "life_cycle" => "50-100 años",
                "climatic_zone" => "Mediterráneo"
            ]
        ];
    }
}

<?php

namespace Tests\Feature\Fruits;

use App\Dictionary\UrlDictionary;
use App\Services\GraphQLClientService;
use App\Services\PathService;
use Mockery\MockInterface;
use Tests\Mockery\Fruits\FruitMock;
use Tests\TestCase;

class FruitControllerTest extends TestCase
{
    use FruitMock;

    public function test_should_get_list_of_fruits_from_api()
    {
        // GIVEN
        $jsonString = PathService::getJsonQueries("fruit", "fruits");
        $data = json_decode($jsonString, true);

        $this->mock(
            GraphQLClientService::class,
            function (MockInterface $mock) use ($data) {
                $mock->shouldReceive('sendRequest')
                    ->once()
                    ->with($data)
                    ->andReturn($this->fruits());
            }
        );

        // WHEN
        $response = $this->get(UrlDictionary::DEFAULT_FRUIT_END_POINT_URL);

        // THEN
        $response->assertJson($this->fruitsResult());
        $response->assertStatus(200);
    }

    public function test_should_store_fruit_in_api()
    {
        // GIVEN
        $fruit = $this->fruit();

        $jsonString = PathService::getJsonQueries("fruit", "addFruit");
        $data = json_decode($jsonString, true);

        $data['variables'] = $fruit;

        $this->mock(
            GraphQLClientService::class,
            function (MockInterface $mock) use ($data) {
                $mock->shouldReceive('sendRequest')
                    ->once()
                    ->with($data)
                    ->andReturn($this->addFruitResult());
            }
        );

        // WHEN
        $response = $this->post(UrlDictionary::DEFAULT_FRUIT_END_POINT_URL, $fruit);

        // THEN
        $response->assertJson($fruit);
        $response->assertStatus(200);
    }

    public function test_should_show_fruit_by_id_from_api()
    {
        // GIVEN
        $id = 1;

        $jsonString = PathService::getJsonQueries("fruit", "fruit");
        $data = json_decode($jsonString, true);

        $data['variables'] = ["id" => $id];

        $this->mock(
            GraphQLClientService::class,
            function (MockInterface $mock) use ($data) {
                $mock->shouldReceive('sendRequest')
                    ->once()
                    ->with($data)
                    ->andReturn($this->fruitResult());
            }
        );

        // WHEN
        $response = $this->get(UrlDictionary::DEFAULT_FRUIT_END_POINT_URL.'/'.$id);

        // THEN
        $response->assertJson($this->fruit());
        $response->assertStatus(200);
    }

    public function test_should_update_fruit_by_id_in_api()
    {
        // GIVEN
        $id = 1;
        $fruit = $this->fruit();
        $fruit['id'] = $id;

        $jsonString = PathService::getJsonQueries("fruit", "updateFruit");
        $data = json_decode($jsonString, true);

        $data['variables'] = $fruit;

        $this->mock(
            GraphQLClientService::class,
            function (MockInterface $mock) use ($data) {
                $mock->shouldReceive('sendRequest')
                    ->once()
                    ->with($data)
                    ->andReturn($this->updateFruitResult());
            }
        );

        // WHEN
        $response = $this->put(UrlDictionary::DEFAULT_FRUIT_END_POINT_URL.'/'.$id, $fruit);

        // THEN
        $response->assertJson($fruit);
        $response->assertStatus(200);
    }

    public function test_should_delete_fruit_by_id_in_api()
    {
        // GIVEN
        $id = 1;
        $deleteFruitResult = $this->deleteFruitResult();

        $jsonString = PathService::getJsonQueries("fruit", "deleteFruit");
        $data = json_decode($jsonString, true);

        $data['variables'] = ["id" => $id];

        $this->mock(
            GraphQLClientService::class,
            function (MockInterface $mock) use ($data, $deleteFruitResult) {
                $mock->shouldReceive('sendRequest')
                    ->once()
                    ->with($data)
                    ->andReturn($deleteFruitResult);
            }
        );

        // WHEN
        $response = $this->delete(UrlDictionary::DEFAULT_FRUIT_END_POINT_URL.'/'.$id);

        // THEN
        $response->assertJson($deleteFruitResult["data"]["deleteFruit"]);
        $response->assertStatus(200);
    }
}

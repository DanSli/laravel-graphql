<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fruits', function (Blueprint $table) {
            $table->id();
            $table->string('scientific_name');
            $table->string('tree_name');
            $table->string('fruit_name');
            $table->string('family');
            $table->string('origin');
            $table->string('description');
            $table->string('bloom');
            $table->string('maturation_fruit');
            $table->string('life_cycle');
            $table->string('climatic_zone');
            $table->string('producing_countries');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fruits');
    }
};

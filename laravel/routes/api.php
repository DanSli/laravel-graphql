<?php

use App\Http\Controllers\CityController;
use App\Http\Controllers\FruitController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('fruits', FruitController::class);

Route::prefix('cities')->group(function() {
    Route::get('/by-name', [CityController::class, 'getCityByName']);
    Route::get('/by-id', [CityController::class, 'getCityById']);
});

### Classes:

- Laravel-GraphQL/laravel/app/Abstracts
- Laravel-GraphQL/laravel/app/Interfaces
- Laravel-GraphQL/laravel/app/Processes
- Laravel-GraphQL/laravel/app/Repositories
- Laravel-GraphQL/laravel/app/DTO
- Laravel-GraphQL/laravel/app/Http/Resources

### External Api

https://fruits-api.netlify.app/graphql

### Documentation:

http://localhost:8000/api/documentation

Generate Doc: ``php artisan l5-swagger:generate``

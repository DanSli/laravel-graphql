<?php

namespace App\Processes\City;

use App\Abstracts\AbstractCityProcess;
use App\DTO\Cities\GetCityById\GetCityByIdResponseDto;
use App\Services\PathService;
use GuzzleHttp\Exception\GuzzleException;
use \Spatie\DataTransferObject\Exceptions\UnknownProperties;

class GetCityByIdProcess extends AbstractCityProcess
{
    /**
     * @param array $values
     * @return GetCityByIdResponseDto
     * @throws GuzzleException
     * @throws UnknownProperties
     */
    public function process(array $values = []): GetCityByIdResponseDto
    {
        $jsonString = PathService::getJsonQueries(self::TYPE, "getCityById");

        $data = json_decode($jsonString, true);

        $data['variables'] = $values;

        $cityResponse = $this->client->sendRequest(self::URL, $data);

        return new GetCityByIdResponseDto($cityResponse);
    }
}

<?php

namespace App\Processes\City;

use App\Abstracts\AbstractCityProcess;
use App\DTO\Cities\GetCityByName\GetCityByNameResponseDto;
use App\Services\PathService;
use GuzzleHttp\Exception\GuzzleException;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class GetCityByNameProcess extends AbstractCityProcess
{
    /**
     * @param array $values
     * @return GetCityByNameResponseDto
     * @throws GuzzleException
     * @throws UnknownProperties
     */
    public function process(array $values = []): GetCityByNameResponseDto
    {
        $jsonString = PathService::getJsonQueries(self::TYPE, "getCityByName");

        $data = json_decode($jsonString, true);

        $data['variables'] = $values;

        $cityResponse = $this->client->sendRequest(self::URL, $data);

        return new GetCityByNameResponseDto($cityResponse);
    }
}

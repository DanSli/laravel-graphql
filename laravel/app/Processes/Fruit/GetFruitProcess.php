<?php

namespace App\Processes\Fruit;

use App\Abstracts\AbstractFruitProcess;
use App\DTO\Fruits\FruitResponseDto;
use App\Services\PathService;
use GuzzleHttp\Exception\GuzzleException;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class GetFruitProcess extends AbstractFruitProcess
{
    /**
     * @param array $values
     * @return FruitResponseDto
     * @throws GuzzleException
     * @throws UnknownProperties
     */
    public function process(array $values = []): FruitResponseDto
    {
        $jsonString = PathService::getJsonQueries(self::TYPE, "fruit");

        $data = json_decode($jsonString, true);

        $data['variables'] = $values;

        $fruitResponse = $this->client->sendRequest(self::URL, $data);

        return new FruitResponseDto($fruitResponse);
    }
}

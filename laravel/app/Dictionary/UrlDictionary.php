<?php

namespace App\Dictionary;

class UrlDictionary
{
    /**
     * FRUITS
     */
    public const DEFAULT_FRUIT_END_POINT_URL = '/api/fruits';
    public const DEFAULT_FRUIT_API_URL = 'https://fruits-api.netlify.app/graphql';

    /**
     * CITIES
     */
    public const DEFAULT_CITY_END_POINT_URL = '/api/cities';
    public const DEFAULT_CITY_API_URL = 'https://graphql-weather-api.herokuapp.com/';
}

<?php

namespace App\Dictionary;

class TypeDictionary
{
    public const DEFAULT_CITY_TYPE = "city";
    public const DEFAULT_FRUIT_TYPE = "fruit";
}

<?php

namespace App\Abstracts;

use App\Dictionary\TypeDictionary;
use App\Dictionary\UrlDictionary;
use App\Interfaces\CityProcessInterface;
use App\Services\GraphQLClientService;

abstract class AbstractCityProcess implements CityProcessInterface
{
    protected const TYPE = TypeDictionary::DEFAULT_CITY_TYPE;
    protected const URL = UrlDictionary::DEFAULT_CITY_API_URL;

    /**
     * @var GraphQLClientService
     */
    protected GraphQLClientService $client;

    /**
     * @param GraphQLClientService $client
     */
    public function __construct(
        GraphQLClientService $client,
    ) {
        $this->client = $client;
    }

    abstract public function process(array $values = []);
}

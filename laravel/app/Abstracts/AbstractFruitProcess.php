<?php

namespace App\Abstracts;

use App\Dictionary\TypeDictionary;
use App\Dictionary\UrlDictionary;
use App\DTO\Fruits\FruitResponseDto;
use App\Interfaces\FruitProcessInterface;
use App\Services\GraphQLClientService;

abstract class AbstractFruitProcess implements FruitProcessInterface
{
    protected const TYPE = TypeDictionary::DEFAULT_FRUIT_TYPE;
    protected const URL = UrlDictionary::DEFAULT_CITY_API_URL;

    /**
     * @var GraphQLClientService
     */
    protected GraphQLClientService $client;

    /**
     * @param GraphQLClientService $client
     */
    public function __construct(
        GraphQLClientService $client,
    ) {
        $this->client = $client;
    }

    /**
     * @param array $values
     * @return FruitResponseDto
     */
    abstract public function process(array $values = []): FruitResponseDto;
}

<?php

namespace App\Interfaces;

use App\DTO\Fruits\FruitResponseDto;

interface FruitProcessInterface
{
    /**
     * @param array $values
     * @return FruitResponseDto
     */
    public function process(array $values = []): FruitResponseDto;
}

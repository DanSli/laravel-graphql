<?php

namespace App\Interfaces;

interface CityProcessInterface
{
    public function process(array $values = []);
}

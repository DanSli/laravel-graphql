<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class GraphQLClientService
{
    /**
     * @param string $url
     * @param array $body
     * @return array
     * @throws GuzzleException
     */
    public function sendRequest(string $url, array $body): array
    {
        $client = new Client();

        $response = $client->request('post', $url, [
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'body' => json_encode($body)
        ]);

        $result = $response->getBody()->getContents();

        $arrayResult = json_decode($result, true);

        if(isset($arrayResult['errors'])) {
            abort(500, $arrayResult['errors'][0]['message']);
        }

        return $arrayResult;
    }
}

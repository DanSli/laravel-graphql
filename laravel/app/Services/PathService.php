<?php

namespace App\Services;

class PathService
{
    private const DEFAULT_PATH = "resources/json/";
    private const DEFAULT_PATH_QUERIES = "/queries/";

    public static function getJsonQueries($type, $file): string
    {
        return file_get_contents(
            base_path(
                self::DEFAULT_PATH.$type.self::DEFAULT_PATH_QUERIES.$file.".json"
            )
        );
    }
}

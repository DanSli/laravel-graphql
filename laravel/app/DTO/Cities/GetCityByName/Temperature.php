<?php

namespace App\DTO\Cities\GetCityByName;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class Temperature extends DataTransferObject
{
	public float $actual;
	public float $feelsLike;
	public float $min;
	public float $max;
}

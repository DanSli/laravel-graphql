<?php

namespace App\DTO\Cities\GetCityByName;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class Data extends DataTransferObject
{
	public ?GetCityByName $getCityByName;
}

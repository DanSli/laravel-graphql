<?php

namespace App\DTO\Cities\GetCityByName;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class GetCityByName extends DataTransferObject
{
	public string $id;
	public string $name;
	public string $country;
	public Coord $coord;
	public Weather $weather;
}

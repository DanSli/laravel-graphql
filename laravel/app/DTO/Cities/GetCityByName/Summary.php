<?php

namespace App\DTO\Cities\GetCityByName;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class Summary extends DataTransferObject
{
	public string $title;
	public string $description;
	public string $icon;
}

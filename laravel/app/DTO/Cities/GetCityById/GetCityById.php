<?php

namespace App\DTO\Cities\GetCityById;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class GetCityById extends DataTransferObject
{
	public string $id;
	public string $name;
	public string $country;
	public Coord $coord;
	public Weather $weather;
}

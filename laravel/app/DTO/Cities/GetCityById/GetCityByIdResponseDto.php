<?php

namespace App\DTO\Cities\GetCityById;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class GetCityByIdResponseDto extends DataTransferObject
{
	public Data $data;
}

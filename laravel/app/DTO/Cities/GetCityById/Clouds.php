<?php

namespace App\DTO\Cities\GetCityById;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class Clouds extends DataTransferObject
{
	public int $all;
	public int $visibility;
	public int $humidity;
}

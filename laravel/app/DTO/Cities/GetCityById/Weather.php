<?php

namespace App\DTO\Cities\GetCityById;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class Weather extends DataTransferObject
{
	public Summary $summary;
	public Temperature $temperature;
	public Wind $wind;
	public Clouds $clouds;
	public int $timestamp;
}

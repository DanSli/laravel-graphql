<?php

namespace App\DTO\Cities\GetCityById;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class Wind extends DataTransferObject
{
	public float $speed;
	public int $deg;
}

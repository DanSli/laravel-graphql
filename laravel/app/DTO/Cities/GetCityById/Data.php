<?php

namespace App\DTO\Cities\GetCityById;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class Data extends DataTransferObject
{
    /** @var GetCityById[]|null */
    #[CastWith(ArrayCaster::class, itemType: GetCityById::class)]
	public ?array $getCityById;
}

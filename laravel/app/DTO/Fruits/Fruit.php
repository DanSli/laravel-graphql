<?php

namespace App\DTO\Fruits;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class Fruit extends DataTransferObject
{
	public string|null $id;
	public string|null $scientific_name;
	public string|null $tree_name;
	public string|null $fruit_name;
	public string|null $family;
	public string|null $origin;
	public string|null $description;
	public string|null $bloom;
	public string|null $maturation_fruit;
	public string|null $life_cycle;
	public string|null $climatic_zone;

    /** @var ProducingCountries[]|null */
    #[CastWith(ArrayCaster::class, itemType: ProducingCountries::class)]
    public ?array $producing_countries;
}

<?php

namespace App\DTO\Fruits;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class FruitResponseDto extends DataTransferObject
{
	public Data $data;
}

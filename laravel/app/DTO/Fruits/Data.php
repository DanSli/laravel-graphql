<?php

namespace App\DTO\Fruits;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Attributes\MapTo;
use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class Data extends DataTransferObject
{
    /** @var Fruit[]|null */
    #[CastWith(ArrayCaster::class, itemType: Fruit::class)]
	public ?array $fruits;

    /**
     * @var Fruit|null
     */
    public ?Fruit $addFruit;

    /**
     * @var Fruit|null
     */
    public ?Fruit $updateFruit;

    /**
     * @var Fruit|null
     */
    public ?Fruit $fruit;

    /**
     * @var Fruit|null
     */
    public ?Fruit $deleteFruit;
}

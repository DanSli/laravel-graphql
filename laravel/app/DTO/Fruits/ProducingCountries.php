<?php

namespace App\DTO\Fruits;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class ProducingCountries extends DataTransferObject
{
    public string $country;
}

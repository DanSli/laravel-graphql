<?php

namespace App\Http\Requests\Fruit;

use Illuminate\Foundation\Http\FormRequest;

class StoreFruitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'scientific_name' => 'required|string',
            'tree_name' => 'required|string',
            'fruit_name' => 'required|string',
            'family' => 'required|string',
            'origin' => 'required|string',
            'description' => 'required|string',
            'bloom' => 'required|string',
            'maturation_fruit' => 'required|string',
            'life_cycle' => 'required|string',
            'climatic_zone' => 'required|string'
        ];
    }
}

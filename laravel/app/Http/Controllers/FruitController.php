<?php

namespace App\Http\Controllers;

use App\Http\Requests\Fruit\StoreFruitRequest;
use App\Http\Requests\Fruit\UpdateFruitRequest;
use App\Http\Resources\FruitResource;
use App\Repositories\FruitRepository;
use Illuminate\Http\Response;

class FruitController extends Controller
{
    /**
     * @var FruitRepository
     */
    private FruitRepository $fruitRepository;

    /**
     * @param FruitRepository $fruitRepository
     */
    public function __construct(FruitRepository $fruitRepository)
    {
        $this->fruitRepository = $fruitRepository;
    }

    /**
     *
     * @OA\Get(
     *      path="/fruits",
     *      operationId="getFruitsList",
     *      tags={"Fruits"},
     *      summary="Get list of fruits",
     *      description="Display a listing of the resource.",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  type="array",
     *                  property="0",
     *                  @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="number", example=1),
     *                      @OA\Property(property="scientific_name", type="string", example="Malus Domestica"),
     *                      @OA\Property(property="tree_name", type="string", example="Manzano"),
     *                      @OA\Property(property="fruit_name", type="string", example="Manzana"),
     *                      @OA\Property(property="family", type="string", example="Rosaceae"),
     *                      @OA\Property(property="origin", type="string", example="Asia Central"),
     *                      @OA\Property(property="description", type="string", example="La RosaceaeRosaceaemanzana es el fruto del manzano, árbol de la familia de las rosáceas. Es una fruta pomácea de forma redonda y sabor más o menos dulce, dependiendo de la variedad. La manzana es un árbol caducifolio, generalmente de 2 a 4,5 m (6 a 15 pies) de altura en cultivo y hasta 9 m (30 pies) en la naturaleza."),
     *                      @OA\Property(property="bloom", type="string", example="Primavera"),
     *                      @OA\Property(property="maturation_fruit", type="string", example="Finales del verano o otoño"),
     *                      @OA\Property(property="life_cycle", type="string", example="60-80 años"),
     *                      @OA\Property(property="climatic_zone", type="string", example="Frio"),
     *                  )
     *              )
     *          )
     *       ),
     *     )
     */
    public function index(): Response
    {
        $result = $this->fruitRepository->getFruits();

        return response(
            FruitResource::collection(
                $result->data->fruits
            )
        );
    }

    /**
     *
     * @OA\Post(
     *      path="/fruits",
     *      operationId="postFruitList",
     *      tags={"Fruits"},
     *      summary="Store fruit to api",
     *      description="Store a newly created resource in storage.",
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      type="object",
     *                      @OA\Property(property="id", type="int"),
     *                      @OA\Property(property="scientific_name", type="string"),
     *                      @OA\Property(property="tree_name", type="string"),
     *                      @OA\Property(property="fruit_name", type="string"),
     *                      @OA\Property(property="family", type="string"),
     *                      @OA\Property(property="origin", type="string"),
     *                      @OA\Property(property="description", type="string"),
     *                      @OA\Property(property="bloom", type="string"),
     *                      @OA\Property(property="maturation_fruit", type="string"),
     *                      @OA\Property(property="life_cycle", type="string"),
     *                      @OA\Property(property="climatic_zone", type="string"),
     *                  ),
     *                  example={
     *                      "id": 1,
     *                      "scientific_name": "Malus Domestica",
     *                      "tree_name": "Manzana",
     *                      "fruit_name": "Manzana",
     *                      "family": "Rosaceae",
     *                      "origin": "Asia Central",
     *                      "description": "La RosaceaeRosaceaemanzana es el fruto del manzano, árbol de la familia de las rosáceas. Es una fruta pomácea de forma redonda y sabor más o menos dulce, dependiendo de la variedad. La manzana es un árbol caducifolio, generalmente de 2 a 4,5 m (6 a 15 pies) de altura en cultivo y hasta 9 m (30 pies) en la naturaleza.",
     *                      "bloom": "Primavera",
     *                      "maturation_fruit": "Finales del verano o otoño",
     *                      "life_cycle": "60-80 años",
     *                      "climatic_zone": "Frio",
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="id", type="number", example=1),
     *              @OA\Property(property="scientific_name", type="string", example="Malus Domestica"),
     *              @OA\Property(property="tree_name", type="string", example="Manzano"),
     *              @OA\Property(property="fruit_name", type="string", example="Manzana"),
     *              @OA\Property(property="family", type="string", example="Rosaceae"),
     *              @OA\Property(property="origin", type="string", example="Asia Central"),
     *              @OA\Property(property="description", type="string", example="La RosaceaeRosaceaemanzana es el fruto del manzano, árbol de la familia de las rosáceas. Es una fruta pomácea de forma redonda y sabor más o menos dulce, dependiendo de la variedad. La manzana es un árbol caducifolio, generalmente de 2 a 4,5 m (6 a 15 pies) de altura en cultivo y hasta 9 m (30 pies) en la naturaleza."),
     *              @OA\Property(property="bloom", type="string", example="Primavera"),
     *              @OA\Property(property="maturation_fruit", type="string", example="Finales del verano o otoño"),
     *              @OA\Property(property="life_cycle", type="string", example="60-80 años"),
     *              @OA\Property(property="climatic_zone", type="string", example="Frio"),
     *          )
     *       ),
     *     )
     */
    public function store(StoreFruitRequest $request): Response
    {
        $result = $this->fruitRepository->addFruit($request->all());

        return response(
            FruitResource::make(
                $result->data->addFruit
            )
        );
    }

    /**
     *
     * @OA\Get(
     *      path="/fruits/{id}",
     *      operationId="getFruitList",
     *      tags={"Fruits"},
     *      summary="Show fruit from api",
     *      description="Display the specified resource.",
     *      @OA\Parameter(
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="id", type="number", example=1),
     *              @OA\Property(property="scientific_name", type="string", example="Malus Domestica"),
     *              @OA\Property(property="tree_name", type="string", example="Manzano"),
     *              @OA\Property(property="fruit_name", type="string", example="Manzana"),
     *              @OA\Property(property="family", type="string", example="Rosaceae"),
     *              @OA\Property(property="origin", type="string", example="Asia Central"),
     *              @OA\Property(property="description", type="string", example="La RosaceaeRosaceaemanzana es el fruto del manzano, árbol de la familia de las rosáceas. Es una fruta pomácea de forma redonda y sabor más o menos dulce, dependiendo de la variedad. La manzana es un árbol caducifolio, generalmente de 2 a 4,5 m (6 a 15 pies) de altura en cultivo y hasta 9 m (30 pies) en la naturaleza."),
     *              @OA\Property(property="bloom", type="string", example="Primavera"),
     *              @OA\Property(property="maturation_fruit", type="string", example="Finales del verano o otoño"),
     *              @OA\Property(property="life_cycle", type="string", example="60-80 años"),
     *              @OA\Property(property="climatic_zone", type="string", example="Frio"),
     *          )
     *       ),
     *     )
     */
    public function show(int $id): Response
    {
        $values = [
            'id' => $id
        ];

        $result = $this->fruitRepository->getFruit($values);

        return response(
            FruitResource::make(
                $result->data->fruit
            )
        );
    }

    /**
     *
     * @OA\Put(
     *      path="/fruits/{id}",
     *      operationId="putFruitList",
     *      tags={"Fruits"},
     *      summary="update fruit in api",
     *      description="Update the specified resource in storage.",
     *          *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      type="object",
     *                      @OA\Property(property="scientific_name", type="string"),
     *                      @OA\Property(property="tree_name", type="string"),
     *                      @OA\Property(property="fruit_name", type="string"),
     *                      @OA\Property(property="family", type="string"),
     *                      @OA\Property(property="origin", type="string"),
     *                      @OA\Property(property="description", type="string"),
     *                      @OA\Property(property="bloom", type="string"),
     *                      @OA\Property(property="maturation_fruit", type="string"),
     *                      @OA\Property(property="life_cycle", type="string"),
     *                      @OA\Property(property="climatic_zone", type="string"),
     *                  ),
     *                  example={
     *                      "scientific_name": "Malus Domestica",
     *                      "tree_name": "Manzana",
     *                      "fruit_name": "Manzana",
     *                      "family": "Rosaceae",
     *                      "origin": "Asia Central",
     *                      "description": "La RosaceaeRosaceaemanzana es el fruto del manzano, árbol de la familia de las rosáceas. Es una fruta pomácea de forma redonda y sabor más o menos dulce, dependiendo de la variedad. La manzana es un árbol caducifolio, generalmente de 2 a 4,5 m (6 a 15 pies) de altura en cultivo y hasta 9 m (30 pies) en la naturaleza.",
     *                      "bloom": "Primavera",
     *                      "maturation_fruit": "Finales del verano o otoño",
     *                      "life_cycle": "60-80 años",
     *                      "climatic_zone": "Frio",
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Parameter(
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="id", type="number", example=1),
     *              @OA\Property(property="scientific_name", type="string", example="Malus Domestica"),
     *              @OA\Property(property="tree_name", type="string", example="Manzano"),
     *              @OA\Property(property="fruit_name", type="string", example="Manzana"),
     *              @OA\Property(property="family", type="string", example="Rosaceae"),
     *              @OA\Property(property="origin", type="string", example="Asia Central"),
     *              @OA\Property(property="description", type="string", example="La RosaceaeRosaceaemanzana es el fruto del manzano, árbol de la familia de las rosáceas. Es una fruta pomácea de forma redonda y sabor más o menos dulce, dependiendo de la variedad. La manzana es un árbol caducifolio, generalmente de 2 a 4,5 m (6 a 15 pies) de altura en cultivo y hasta 9 m (30 pies) en la naturaleza."),
     *              @OA\Property(property="bloom", type="string", example="Primavera"),
     *              @OA\Property(property="maturation_fruit", type="string", example="Finales del verano o otoño"),
     *              @OA\Property(property="life_cycle", type="string", example="60-80 años"),
     *              @OA\Property(property="climatic_zone", type="string", example="Frio"),
     *          )
     *       ),
     *     )
     */
    public function update(UpdateFruitRequest $request, int $id): Response
    {
        $values = $request->all();

        $values['id'] = $id;

        $result = $this->fruitRepository->updateFruit($values);

        return response(
            FruitResource::make(
                $result->data->updateFruit
            )
        );
    }

    /**
     *
     * @OA\Delete (
     *      path="/fruits/{id}",
     *      operationId="deleteFruitList",
     *      tags={"Fruits"},
     *      summary="delete fruit in api",
     *      description="Remove the specified resource from storage.",
     *      @OA\Parameter(
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="id", type="number", example=1),
     *              @OA\Property(property="scientific_name", type="string", example="Malus Domestica"),
     *              @OA\Property(property="tree_name", type="string", example="Manzano"),
     *              @OA\Property(property="fruit_name", type="string", example="Manzana"),
     *              @OA\Property(property="family", type="string", example="Rosaceae"),
     *              @OA\Property(property="origin", type="string", example="Asia Central"),
     *              @OA\Property(property="description", type="string", example="La RosaceaeRosaceaemanzana es el fruto del manzano, árbol de la familia de las rosáceas. Es una fruta pomácea de forma redonda y sabor más o menos dulce, dependiendo de la variedad. La manzana es un árbol caducifolio, generalmente de 2 a 4,5 m (6 a 15 pies) de altura en cultivo y hasta 9 m (30 pies) en la naturaleza."),
     *              @OA\Property(property="bloom", type="string", example="Primavera"),
     *              @OA\Property(property="maturation_fruit", type="string", example="Finales del verano o otoño"),
     *              @OA\Property(property="life_cycle", type="string", example="60-80 años"),
     *              @OA\Property(property="climatic_zone", type="string", example="Frio"),
     *          )
     *       ),
     *     )
     */
    public function destroy(int $id): Response
    {
        $values = [
            'id' => $id
        ];

        $result = $this->fruitRepository->deleteFruit($values);

        return response(
            FruitResource::make(
                $result->data->deleteFruit
            )
        );
    }
}

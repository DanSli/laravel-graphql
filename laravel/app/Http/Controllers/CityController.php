<?php

namespace App\Http\Controllers;
use App\Http\Requests\City\GetCityByIdRequest;
use App\Http\Requests\City\GetCityByNameRequest;
use App\Http\Resources\CityResource;
use App\Repositories\CityRepository;
use Illuminate\Http\Response;

class CityController extends Controller
{
    /**
     * @var CityRepository
     */
    private CityRepository $cityRepository;

    /**
     * @param CityRepository $cityRepository
     */
    public function __construct(CityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    /**
     * @param GetCityByNameRequest $request
     * @return Response
     */
    public function getCityByName(GetCityByNameRequest $request): Response
    {
        $result = $this->cityRepository->getCityByName(
            $request->all()
        );

        $city = $result->data->getCityByName;

        if($city === null) {
            return response($city);
        }

        return response(
            CityResource::make(
                $city
            )
        );
    }

    /**
     * @param GetCityByIdRequest $request
     * @return Response
     */
    public function getCityById(GetCityByIdRequest $request): Response
    {
        $result = $this->cityRepository->getCityById(
            $request->all()
        );

        $city = $result->data->getCityById;

        if($city === null) {
            return response($city);
        }

        return response(
            CityResource::collection(
                $city
            )
        );
    }
}

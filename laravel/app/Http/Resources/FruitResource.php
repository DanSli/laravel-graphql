<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FruitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'scientific_name' => $this->scientific_name,
            'tree_name' => $this->tree_name,
            'fruit_name' => $this->fruit_name,
            'family' => $this->family,
            'origin' => $this->origin,
            'description' => $this->description,
            'bloom' => $this->bloom,
            'maturation_fruit' => $this->maturation_fruit,
            'life_cycle' => $this->life_cycle,
            'climatic_zone' => $this->climatic_zone,
            // 'producing_countries' => $this->producing_countries
        ];
    }
}

<?php

namespace App\Repositories;

use App\DTO\Fruits\FruitResponseDto;
use App\Processes\Fruit\AddFruitProcess;
use App\Processes\Fruit\DeleteFruitProcess;
use App\Processes\Fruit\GetFruitProcess;
use App\Processes\Fruit\GetFruitsProcess;
use App\Processes\Fruit\UpdateFruitProcess;

class FruitRepository
{
    /**
     * @return FruitResponseDto
     */
    public function getFruits(): FruitResponseDto
    {
        $fruitProcess = app(GetFruitsProcess::class);

        return $fruitProcess->process();
    }

    /**
     * @param array $values
     * @return FruitResponseDto
     */
    public function getFruit(array $values): FruitResponseDto
    {
        $fruitProcess = app(GetFruitProcess::class);

        return $fruitProcess->process($values);
    }

    /**
     * @param array $values
     * @return FruitResponseDto
     */
    public function addFruit(array $values): FruitResponseDto
    {
        $fruitProcess = app(AddFruitProcess::Class);

        return $fruitProcess->process($values);
    }

    /**
     * @param array $values
     * @return FruitResponseDto
     */
    public function updateFruit(array $values): FruitResponseDto
    {
        $fruitProcess = app(UpdateFruitProcess::class);

        return $fruitProcess->process($values);
    }

    /**
     * @param array $values
     * @return FruitResponseDto
     */
    public function deleteFruit(array $values): FruitResponseDto
    {
        $fruitProcess = app(DeleteFruitProcess::class);

        return $fruitProcess->process($values);
    }
}

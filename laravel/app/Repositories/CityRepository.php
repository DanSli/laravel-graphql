<?php

namespace App\Repositories;

use App\Processes\City\GetCityByIdProcess;
use App\Processes\City\GetCityByNameProcess;

class CityRepository
{
    public function getCityByName(array $values)
    {
        $fruitProcess = app(GetCityByNameProcess::Class);

        return $fruitProcess->process($values);
    }

    public function getCityById(array $values)
    {
        $fruitProcess = app(GetCityByIdProcess::Class);

        return $fruitProcess->process($values);
    }
}
